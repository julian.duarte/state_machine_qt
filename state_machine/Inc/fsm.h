/**
  ******************************************************************************
  * @file    fsm.h
  * @author  julian.duarte@titoma.com
  * @brief   Header for the generic Finite State Machine module.
  ******************************************************************************
  */
#ifndef FSM_H
#define FSM_H

#include <QObject>

class fsm : public QObject
{
    Q_OBJECT

public:
    fsm(QObject *parent = nullptr);

};

#endif // FSM_H
