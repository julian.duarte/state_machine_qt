#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStateMachine>
#include <QState>
#include <QFinalState>
#include <QPushButton>
#include <QCoreApplication>
#include <QDebug>
#include <QSignalTransition>
#include <QMessageBox>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class PingEvent : public QEvent
{
public:
    PingEvent() : QEvent(QEvent::Type(QEvent::User+2))
        {}
};

class PongEvent : public QEvent
{
public:
    PongEvent() : QEvent(QEvent::Type(QEvent::User+3))
        {}
};

class Pinger : public QState
{
public:
    Pinger(QState *parent)
        : QState(parent) {}

protected:
    void onEntry(QEvent *) override
    {
        machine()->postEvent(new PingEvent());
        fprintf(stdout, "ping?\n");
    }
};


class PingTransition : public QAbstractTransition
{
public:
    PingTransition() {}

protected:
    bool eventTest(QEvent *e) override {
        return (e->type() == QEvent::User+2);
    }
    void onTransition(QEvent *) override
    {
        machine()->postDelayedEvent(new PongEvent(), 500);
        fprintf(stdout, "pong!\n");
    }
};

class PongTransition : public QAbstractTransition
{
public:
    PongTransition() {}

protected:
    bool eventTest(QEvent *e) override {
        return (e->type() == QEvent::User+3);
    }
    void onTransition(QEvent *) override
    {
        machine()->postDelayedEvent(new PingEvent(), 500);
        fprintf(stdout, "ping?\n");
    }
};


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void state_machine_definition_state();
    void state_machine_transition_between_states();
    void state_machine_set_up();
    void state_machine_property();
    void state_machine_entered_and_exited();
    void print();

private:
    Ui::MainWindow *ui;

    QStateMachine *machine;
    QState *group;
    Pinger *pinger;
    QState *ponger;


};
#endif // MAINWINDOW_H
