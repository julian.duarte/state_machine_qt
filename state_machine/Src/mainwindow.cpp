#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    state_machine_definition_state();
    state_machine_transition_between_states();
    state_machine_set_up();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::state_machine_definition_state()
{
    machine = new QStateMachine();
    group = new QState(QState::ParallelStates);
    group->setObjectName("group");

    pinger = new Pinger(group);
    pinger->setObjectName("pinger");

    ponger = new QState(group);
    ponger->setObjectName("ponger");

}

void MainWindow::state_machine_transition_between_states()
{
    pinger->addTransition(new PongTransition());
    ponger->addTransition(new PingTransition());
}

void MainWindow::state_machine_set_up()
{
    machine->addState(group);
    machine->setInitialState(group);
    machine->start();
}

void MainWindow::state_machine_property()
{

}

void MainWindow::state_machine_entered_and_exited()
{
}

void MainWindow::print()
{
    qDebug()<<"hola hola";
}
